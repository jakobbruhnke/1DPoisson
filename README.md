# Implementation of a neural network and physics-informed neural network to approximate the solution of a 1D Poisson equation

This code accompanies my bachelor thesis on physics-informed machine learning for optical progagation.

A working Python installation, including tensorflow, numpy and matplotlib is required.
